package screaminggoat.objects;

import java.util.Objects;

public class Slave {

    private String ip;

    public Slave(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }

    @Override
    public String toString() {
        return ip;
    }

    @Override
    public boolean equals(Object obj) {
        return Objects.equals(this.ip, ((Slave) obj).ip);
    }
}
