package screaminggoat.handlers;

import com.google.gson.Gson;
import org.eclipse.jetty.websocket.api.StatusCode;
import screaminggoat.SlaveStore;
import screaminggoat.objects.Slave;
import spark.Request;
import spark.Response;
import spark.Route;
import sun.security.provider.certpath.OCSPResponse;

public class RegisterSlaveHandler implements Route {

    private SlaveStore store;

    public RegisterSlaveHandler(SlaveStore store) {
        this.store = store;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {

        Slave slave = new Gson().fromJson(request.body(), Slave.class);

        if (store.hasSlave(slave)) {
            response.status(409); // Conflict
            return "IP already registered";
        }

        store.addSlave(slave);
        return "HELLO " + slave.getIp();
    }
}
