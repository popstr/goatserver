package screaminggoat;

import screaminggoat.handlers.RegisterSlaveHandler;
import spark.Spark;

public class Server {
    public static void main(String[] args) {

        SlaveStore slaveStore = new SlaveStore();

        Spark.port(8080);
        Spark.get("/hello", (req, res) -> "Hello World");

        RegisterSlaveHandler registerSlaveHandler = new RegisterSlaveHandler(slaveStore);
        Spark.post("/register", registerSlaveHandler);
        Spark.get("/slaves", (req, res) -> slaveStore.getSlaves());


    }
}
