package screaminggoat;

import screaminggoat.objects.Slave;

import java.util.ArrayList;
import java.util.List;

public class SlaveStore {
    private final List<Slave> slaves;

    public SlaveStore() {
        this.slaves = new ArrayList<>();
    }

    public void addSlave(Slave slave) {
        slaves.add(slave);
    }

    public List<Slave> getSlaves() {
        return slaves;
    }

    public boolean hasSlave(Slave slave) {
        return slaves.contains(slave);
    }
}
